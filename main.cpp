#include <iostream>
using namespace std;

int Stepen2(int n,int a)
{
    if (a == n)
    {
        cout<<"YES"<<endl;
        return 0;
    }
    else if( a > n)
    {
        cout<<"NO"<<endl;
        return 0;
    }
    else if( a < n )
    {
        a = a * 2;
        return Stepen2(n,a);
    }
}

int main()
{
    int n;
    cout<<"Введите число: ";
    cin>> n;
    Stepen2(n,2);
    return 0;
}
